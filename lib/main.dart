import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int cIndex = 0;
  final  pages = [
    Center( child: Text('Главная', style: TextStyle(fontSize: 50),),),
    Center( child: Text('Каталог', style: TextStyle(fontSize: 50),),),
    Center( child: Text('Избранное', style: TextStyle(fontSize: 50),),),
    Center( child: Text('Корзина', style: TextStyle(fontSize: 50),),),
    Center( child: Text('Профиль', style: TextStyle(fontSize: 50),),),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Creonit'),
        centerTitle: true,
      ),
      body: Center(
        child: IndexedStack( index: cIndex, children: pages),
      ),
      bottomNavigationBar: BottomNavigationBar(
        
        selectedFontSize: 11,
        unselectedFontSize: 11,
        selectedItemColor: Colors.black,
        currentIndex: cIndex,
        onTap: (index) => setState(() =>  cIndex = index),
        type: BottomNavigationBarType.fixed,
        items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home_outlined),
          label: 'Главная',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.grid_view_outlined),
          label: 'Каталог',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.favorite_outline_outlined),
          label: 'Избранное',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.shopping_cart_outlined),
          label: 'Корзина',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person_outline_outlined),
          label: 'Профиль',
        ),
      ],),
    );
     
  }
}
